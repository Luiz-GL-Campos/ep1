#include "cliente.hpp"
#include "carrinho.hpp"
#include "estoque.hpp"
#include "produto.hpp"

#include <iostream>
#include <string>
#include <stdlib.h>
#include <fstream>

using namespace std;
Cliente *cadastro = new Cliente();

//Menu principal
int main(){
    void menu_estoque(); //outros menus de opções
    void menu_cliente();    
    
    int selecao;  //variavel de seleção para o switch
    
    do{   //looping para não fechar o código enquanto uma opção não for selecionada
        system("clear");
        cout << "   Victoria Shop's" << endl;
        cout << "         Menu" << endl;
        cout << endl;
        cout << "(1)Cliente" << endl;
        cout << "(2)Estoque" << endl;
        cout << endl;
        cout << "(0)sair" << endl;

        cin >> selecao;
        switch (selecao){
            case 0:
                system("clear");  //fechar terminal
                exit(-1);
                break;
                break;

            case 1:
                menu_cliente();
                break;

            case 2:
                menu_estoque();
                break;

        }
        
    }while(selecao < 0 || selecao > 2);

    return 0;
}


//============================================================================


void menu_estoque(){
    string nome_produto = "";
    double quantidade = 0;
    
    Estoque *dados = new Estoque(); //ponteiro para criar novo objeto estoque

    int selecao;
    
    do{
        system("clear");
        cout << "       Estoque" << endl;
        cout << endl;
        cout << "(1)Ver estoque" << endl;
        cout << "(2)Cadastrar novo produto" << endl;
        cout << "(3)Atualizar quantidade de um produto" << endl;
        cout << endl;
        cout << "(0)Voltar para menu principal" << endl;

        cin >> selecao;
        switch (selecao){
            case 0:
                main();
                break;

            case 1:
                system("clear");
                dados -> get_nome_produto();
                cout << endl << endl;
                
                cout << "Aperte enter para continuar." << endl;

                getchar();
                getchar();
                
                main();

                break;

            case 2:
                system("clear");
               
    
                cout << "Insira os dados do produto a ser adicionado no estoque:" << endl;
                cout << "Nome e quantidade respectivamente." << endl;

                cin >> nome_produto >> quantidade;

                dados -> set_dados_produto(nome_produto, quantidade);

                cout << "Aperte enter para continuar." << endl;

                getchar();
                getchar();
                
                main();
                
                break;

            case 3:
                
                main();
                
                break;
        }
        
    }while(selecao < 0 || selecao > 3);
    
  
}


//================================================================================



void menu_cliente(){
    void opcoes_cliente();
    
    string nome = "";
    string rg = "";
    string data_nascimento = "";
    string senha = "";
    bool socio = 0;

    string linha = "";
    
    ifstream verificadados; //arquivo para leitura

    int selecao;
    
    do{
        system("clear");
        cout << "       Cliente" << endl;
        cout << endl;
        cout << "(1)Cadastro novo cliente" << endl;
        cout << "(2)Login Cliente" << endl;
        cout << endl;
        cout << "(0)Voltar para menu principal" << endl;

        cin >> selecao;
        switch (selecao){
            case 0:
                main();
                break;

            case 1:
                system("clear");

                cout << "Insira os dados do cliente a ser adicionado na lista:" << endl;
                cout << "Nome, RG, data de nascimento e senha respectivamente." << endl;

                cin >> nome >> rg >> data_nascimento >> senha;

                cadastro -> set_cliente_dados(nome, rg, data_nascimento, senha);
                
                cout << "Aperte enter para continuar." << endl;

                getchar();
                getchar();
                
                main();

                break;

            case 2: 
                system("clear");

                cout << "Insira seu nome registrado e a senha:" << endl;

                cin >> nome >> senha;

                verificadados.open("doc/"+ nome +".txt",ios::in); //Cria o arquivo com o nome do cliente
                
                if(verificadados.is_open()){ //verifica se o arquivo existe
                    
                    while(! verificadados.eof()){  //looping para verificar linha por linha até o fim do arquivo
                        getline(verificadados, linha);
                        
                        if(linha == "#Senha"){ //verifica o arquivo até achar a senha
                            getline(verificadados, linha);
                            
                            if (linha == senha){ //verifica se a senha está correta
                                cout << "Login efetuado com sucesso." << endl << "Aperte a tecla Enter." << endl;

                                getchar();
                                getchar();

                                opcoes_cliente();

                            }else{
                                //no caso de senha incorreta volta para o menu de clientes
                                cout << "Senha incorreta." << endl << "Aperte a tecla Enter." << endl;

                                getchar();
                                getchar();

                                menu_cliente();
                            }
                        }
                    }
                    



                }else{
                    cout << "Erro: Nome nao registrado." << endl << "Aperte a tecla Enter." << endl;

                    getchar();
                    getchar();

                    menu_cliente();
                }
               

                break;

        }
        
    }while(selecao < 0 || selecao > 2);
}


//==============================================================================


void opcoes_cliente(){
    int selecao;
    
    do{
    
        system("clear");
        cout << "   Bem vindo a Loja da Victoria " << endl;
        cout << "        O que deseja fazer?" << endl;
        cout << "Você pode:" << endl;
        cout << endl;
        cout << "(1)Verificar seus dados" << endl;
        cout << "(2)Verificar produtos disponiveis" << endl;
        cout << "(3)Adicionar ao carrinho" << endl;
        cout << "(4)Confirmar compra" << endl;
        cout << endl;
        cout << "(0)Voltar para menu principal" << endl;

        cin >> selecao;
        switch (selecao){
            case 0:
                main();

                break;

            case 1:
                system("clear");
                        
                cout << cadastro-> get_nome() << endl;
                cout << cadastro-> get_rg() << endl;
                cout << cadastro-> get_data_nascimento() << endl;
                cout << cadastro-> get_senha() << endl;

                cout << endl;
                cout << endl;

                cout << "Aperte enter para continuar." << endl;

                getchar();
                getchar();

                opcoes_cliente();
                break;

            case 2:

                break;

            case 3:

                break;

            case 4:

                break;
        }

    
    }while(selecao < 0 || selecao > 4);
}