#ifndef ESTOQUE_HPP
#define ESTOQUE_HPP

#include <iostream>
#include <string>
using namespace std;

class Estoque {
    private:

    public:
        string nome_produto;
        double quantidade;
        
        Estoque();
        Estoque(double quantidade,string nome_produto);
        ~Estoque();
        
        void set_dados_produto(string nome_produto,double quantidade);
        string get_nome_produto();
        double get_quantidade();
    
};

#endif