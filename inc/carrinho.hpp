#ifndef CARRINHO_HPP
#define CARRINHO_HPP

#include <iostream>
#include <string>
using namespace std;

class Carrinho{
    private:
        string produtos;
        double preco;
        double total;
        bool continuar_compra;  //pode ser retirado se for mais fácil com if else
        bool cancelamento;      //------------
        
    public:
        Carrinho();
        Carrinho(string produtos, double preco, double total, bool continuar_compra, bool cancelamento);
        ~Carrinho();
    
        void set_adicionar(string produtos);
        void get_listaprodutos();
};

#endif