#include "estoque.hpp"
#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <iostream>
#include <string>
using namespace std;

class Produto: public Estoque{
    private:
        string tipo;
        double valor;

    public:
        Produto();
        Produto(string tipo, double valor);
        ~Produto();
        
        void set_tipo(string tipo); 
        void set_valor(double valor);
        
        string get_tipo_produto();
        double get_valor_produto();

        string get_nome_produto();
        double get_quantidade();

};

#endif