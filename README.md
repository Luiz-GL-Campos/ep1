# EP1 - OO 2019.2 (UnB - Gama)

Turmas Renato e Carla
Data de entrega: 01/10/2019

## Minha Ep1 - Instruções

Aviso: Projeto ainda incompleto(possui warnings de funções não usadas).

Funções implemantadas: 
-Cadastro de clientes: cada um possui seu próprio arquivo para registro dos seus dados e do histórico de compra.
-Login Cliente: Com o nome e senha definidos anteriormente é possível fazer o login para as funções do cliente.
-Cadastro de produtos no estoque: nome e quantidade com apenas 1 arquivo para listar todos os produtos.
-Visuaçização de estoque: vê todos os produtos cadastrados no arquivo.

Utiliza make file para compilar e executar.

Obs: Não achei necessário um login de funcionário para cadastrar produtos e suas respectivas características pois na descrição do projeto diz, "**todas as vendas serão realizadas pelo computador operado por uma funcionária**", logo apenas a senha para ligar a própria máquina onde está sendo executado o programa já serviria.